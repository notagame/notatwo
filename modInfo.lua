-- notAlab 2017-01-12
-- @manual: https://springrts.com/wiki/Modinfo.lua

local modinfo = {
	name = "notAtwo",
	shortname = "notAtwo",
	version = "alfa-$VERSION",
	game = "notAtwo",
	shortgame = "notAtwo",
	description = "notAtwo",
	url = "http://notalab.com/",
	modtype = "1",
	depend = {
	
	}
}

return modinfo