-- notAlab 2017-01-22

local options = {
	{
		key = "MaxUnits",
		name = "Max units",
		desc = "Max units per team",
		type = "number",
		def = 600,
		min = 300,
		max = 1200,
		step = 100,
	},
}

return options
  